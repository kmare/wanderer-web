from flask import Flask, render_template, request
from DataDB import DataDB
import json
from geojson import Feature, Point, FeatureCollection


app = Flask(__name__)
d = DataDB()


@app.route('/')
def home():
    return render_template('index.html', title='Come Bike Later')


@app.route('/about/')
def about():
    return render_template('about.html', title='About us')


@app.route('/contact/', methods=['GET', 'POST'])
def contact():
    if request.method == 'GET':
        return render_template('contact.html', title='Contact Us')
    if request.method == 'POST':
        return "Message sent! Thank you!"


@app.route('/gps/', methods=['GET', 'POST'])
def gps():
    if request.method == 'GET':
        data = d.get_all()
        return render_template('gps.html', data=data, title="Recent IoT data")
    if request.method == 'POST':
        data = request.form
        ret = d.insert_coords(data['bike_id'], data['gps_x'], data['gps_y'], data['status'], data['ts'], data['info'])
        j = json.loads(ret)
        print(j, ret)
        return ret


@app.route('/gpsdata', methods=['GET'])
def gps_data():
    data = d.get_all()
    return json.dumps(data, default=str)


@app.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html', title="Login")
    if request.method == 'POST':
        data = request.form
        return "login: " + data['username'] + ", " + data['password']


@app.route('/restricted/customers')
def restricted_customers():
    data = d.get_customers()
    return render_template('restricted/customers.html', data=data, title="Restricted - Customers")


@app.route('/restricted/bikes')
def restricted_bikes():
    data = d.get_bikes_with_customers()
    return render_template('restricted/bikes.html', data=data, title="Restricted - Bikes")


@app.route('/test/', methods=['GET', 'POST'])
def testdata():
    if request.method == 'GET':
        return render_template('testdata.html', title="Testing your data")
    if request.method == 'POST':
        data = request.form
        return "login: " + data['username'] + ", " + data['password']


@app.route('/test/<int:loc_id>')
def test(loc_id):
    return 'testing %d' % loc_id


@app.route('/map_data')
def map_data():
    loc = d.get_customer_locations()
    print(loc)

    feat_array = []

    for f in loc:
        feat = Feature(geometry=Point((f[2], f[1])), properties={"serial": f[3], "x": f[1], "y": f[2], "status": f[4], "customer": f[6]})
        feat_array.append(feat)

    feat_coll = FeatureCollection(feat_array)
    print(feat_coll, type(json.dumps(feat_coll)))
    return json.dumps(feat_coll)


if __name__ == '__main__':
    app.run('0.0.0.0', 8080, threaded=True)
