import psycopg2
import json


class DataDB:
    def __init__(self):
        print("Successful database connection.")
        self.conn = psycopg2.connect(database="cbldb", user="pioann", password="pioann",
                                     host="127.0.0.1", port="5432")
        self.cur = self.conn.cursor()

    def get_all(self):
        self.cur.execute("SELECT * FROM iotdata")
        rows = self.cur.fetchall()
        return rows

    def get_customers(self):
        self.cur.execute("SELECT * FROM customers")
        rows = self.cur.fetchall()
        return rows

    def get_bikes(self):
        self.cur.execute("SELECT * FROM bikes")
        rows = self.cur.fetchall()
        return rows

    def get_bikes_with_customers(self):
        self.cur.execute(
            "SELECT b.bike_id, b.deviceid, c.name, b.status, b.updated_at FROM bikes as b "
            "INNER JOIN customers as c ON b.cust_id = c.cust_id")
        rows = self.cur.fetchall()
        return rows

    def insert_coords(self, bike_id, gps_x, gps_y, status, ts, info):
        self.cur.execute(
            "INSERT INTO iotdata (bike_id, gps_x, gps_y, status, ts, info, created_at) "
            "VALUES (%s, %s, %s, %s, %s, %s, CURRENT_TIMESTAMP) RETURNING *",
            (bike_id, gps_x, gps_y, status, ts, info))
        res = self.cur.fetchone()
        self.conn.commit()

        r = {
            'rec_id': res[0],
            'bike_id': res[1],
            'gps_x': res[2],
            'gps_y': res[3],
            'status': res[4],
            'ts': res[5],
            'info': res[6],
            'created_at': res[7],
        }
        return json.dumps(r, default=str)

    def get_customer_locations(self):
        self.cur.execute(
            "SELECT f.bike_id, f.gps_x, f.gps_y, f.deviceid, f.status, f.ts, c.name "
            "FROM ("
            "SELECT i.bike_id, k.cust_id, k.deviceid, i.status, i.gps_x, i.gps_y, i.ts "
            "FROM ("
            "SELECT m.bike_id, m.gps_x, m.gps_y, m.status, iot.mx, m.ts "
            "FROM ( "
            "SELECT bike_id, max(ts) AS mx "
            "FROM iotdata "
            "GROUP BY bike_id "
            ") iot INNER JOIN iotdata m ON m.bike_id = iot.bike_id AND iot.mx = m.ts "
            ") i INNER JOIN bikes AS k ON i.bike_id = k.bike_id "
            ") f INNER JOIN customers AS c ON f.cust_id = c.cust_id;"
        )
        rows = self.cur.fetchall()
        return rows

    def close(self):
        self.cur.close()
        self.conn.close()
